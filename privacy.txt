Welcome to the Igotcha website, Igotcha delivery (and all pages attaching to and including this domain name) and our applications (each our "Platform"). This page (together with the documents referred to on it) tells you how we store customer data.

Igotcha ("we", "our", "us" or " Igotcha ") is committed to protecting the privacy of all visitors to our app and all visitors who access our website or platform through any mobile application (together, "Site and Platform(s)"). Please read the following privacy policy ("Privacy Policy") carefully which explains how we use and protect your information.

By visiting and/or using our Site and Platform(s), you agree and where required  you consent to the collection, use, transfer, disclosure, retention and protection of your information as set out in this policy (together with any other documents referred to on it).
d
1. CONTACT DETAILS
If you have any queries or requests concerning this privacy policy or how we handle your data more generally, please get in touch with us using the following details.

By contacting our general customer services team at: "igotcha2022@gmail.com"
2. HOW WE COLLECT YOUR INFORMATION
1. We collect your personal information when you interact with us or use our services, such as when you use our app  to place an order. We also look at how visitors use our app, to help us improve our services and optimise customer experience.

2. We collect information:
 when you create an account with us or you change your account settings.  when you place an order with us and during the order process (including for payment and order delivery) when you contact us directly via email, phone, post, message or via our chat function; and  when you browse and use our app (before and after you create an account with us).  We also collect information from third party app

3. INFORMATION THAT WE COLLECT FROM YOU
1. As part of our commitment to the privacy of our customers and visitors to our apps  more generally, we want to be clear about the sorts of information we will collect from you. 2. When you visit the app  or make a Igotcha order through the app, you are asked to provide information about yourself including your name, contact details, delivery address, order details and payment information . 3. We also collect information about your usage of the app and information about you from any messages you post to the app or when you contact us or provide us with feedback, including via e-mail, letter, phone or chat function. If you contact us by phone, we record the call for training and service improvement purposes, and make notes in relation to your call.

4. Igotcha FOR BUSINESS
1. We also process your information to determine whether you may be interested in hearing about our Igotcha for Business service. 2. Where we think you may be interested in our Igotcha for Business service, we may contact you (by email or telephone) using the contact details you have provided to let you know about it. We are the controller of this information. You have the right to opt out of receiving these types of communications and may do so by changing your marketing preferences (as set out in section 7 below), or, in the case of telephone, by contacting us using the details above.
