@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">Countries</h3>
            </div>
        </div>
    </div>
    <div class="body">

    <!-- Tabs -->

        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active"><a href="#home" data-toggle="tab"><h4>{{$lang->get(64)}}</h4></a></li>
            @if ($userinfo->getUserPermission("Faq::Create"))
            <li role="presentation"><a href="#create" data-toggle="tab" ><h4>{{$lang->get(65)}}</h4></a></li>
            @endif
            <li id="tabEdit" style='display:none;' role="presentation"><a href="#edit" data-toggle="tab"><h4>{{$lang->get(66)}}</h4></a></li>
        </ul>

        <!-- Tab List -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="home">
                @if (isset($texton) && $texton == "green")
                    <div class="alert bg-green" >
                        {{$text}}
                    </div>
                @endif
                    <div id="redzone" class="alert bg-red" hidden>
                    </div>

                <div class="row clearfix js-sweetalert">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h3>
                                    Countries List
                                </h3>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable ">
                                        <thead>
                                        <tr>
                                            <th>IMG</th>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>IMG</th>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                            <th>Actions</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>

                                        @foreach($countries as $key => $country)
                                            <tr id="tr{{$country->id}}">
                                                <td><img src="{{$country->flg_url}}" class="img-thumbnail" width="50" height="auto"></td>
                                                <td>{{$country->name}}</td>
                                                <td>{{$country->code}}</td>
                                                <td>{{$country->created_at->format('d M Y @ H:i:s')}}</td>
                                                <td>{{$country->updated_at->format('d M Y @ H:i:s')}}</td>
                                                <td>
                                                    @if ($userinfo->getUserPermission("Faq::Edit"))
                                                    <button type="button" class="btn btn-default waves-effect"
                                                        onclick="editItem('{{$country->id}}','{{$country->name}}', '{{$country->code}}','{{$country->flg_url}}')">
                                                        <img src="img/iconedit.png" width="25px">
                                                    </button>
                                                    @endif
                                                </td>
                                            </tr>

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        <!-- End Tab List -->

        <!-- Tab Create -->

        @include('geography.country.Create')

        <!-- Tab Edit -->

        @include('geography.country.Edit')


    </div>
    </div>

    <script type="text/javascript">

        var form = document.getElementById("formcreate");
        form.addEventListener("submit", checkForm, true);

        function checkForm(event) {
            var alertText = "";
            if (!document.getElementById("question").value) {
                alertText = "<h4>{{$lang->get(316)}}</h4>";
            }
            if (!document.getElementById("answer").value) {
                alertText = alertText+"<h4>{{$lang->get(317)}}</h4>";
            }
            if (alertText != "") {
                var div = document.getElementById("redalert");
                div.innerHTML = '';
                div.style.display = "block";
                var div2 = document.createElement("div");
                div2.innerHTML = alertText;
                div.appendChild(div2);
                window.scrollTo(0, 0);
                event.preventDefault();
                return false;
            }
        }


        var eform = document.getElementById("formedit");
        eform.addEventListener("submit", checkFormEdit, true);

        function checkFormEdit(event) {
            var alertText = "";
            if (!document.getElementById("questionEdit").value) {
                alertText = "<h4>{{$lang->get(316)}}</h4>";
            }
            if (!document.getElementById("answerEdit").value) {
                alertText = alertText+"<h4>{{$lang->get(317)}}</h4>";
            }
            if (alertText != "") {
                var div = document.getElementById("redalertEdit");
                div.innerHTML = '';
                div.style.display = "block";
                var div2 = document.createElement("div");
                div2.innerHTML = alertText;
                div.appendChild(div2);
                window.scrollTo(0, 0);
                event.preventDefault();
                return false;
            }
        }


        function showDeleteMessage(id) {
            swal({
                title: "{{$lang->get(81)}}",
                text: "{{$lang->get(82)}}",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "{{$lang->get(83)}}",
                cancelButtonText: "{{$lang->get(84)}}",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    console.log(id);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        type: 'POST',
                        url: '{{ url("faqdetete") }}',
                        data: {id: id},
                        success: function (data){
                            if (!data.ret){
                                document.getElementById("redzone").innerHTML = data.text;
                                document.getElementById("redzone").style.display = "block";
                                return;
                            }
                            //
                            // remove from ui
                            //
                            var div = document.getElementById('tr'+id);
                            div.remove();
                        },
                        error: function(e) {
                            console.log(e);
                        }}
                    );
                } else {

                }
            });
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href")
            if (target != "#edit")
                document.getElementById("tabEdit").style.display = "none";
            console.log(target);
        });

        async function editItem(id, name, code,flg) {
            document.getElementById("tabEdit").style.display = "block";
            document.getElementById("editid").value = id;
            $('.nav-tabs a[href="#edit"]').tab('show');
            //

            $("#flgSrc").attr("src", flg);
            document.getElementById("nameEdit").value = name;
            document.getElementById("codeEdit").value = code;
        }
    </script>


@endsection

@section('content2')

@endsection
