<div role="tabpanel" class="tab-pane fade" id="edit">

    <div id="redalertEdit" class="alert bg-red" style='display:none;' >

    </div>

    <form id="formedit" method="post" enctype="multipart/form-data"  action="{{route('countries.update')}}">
        @csrf

        <input type="hidden" id="editid" name="id"/>

        <div class="col-md-6 foodm">

            <div class="col-md-12 foodm">
                <div class="col-md-2 foodm">
                    <h4>Name</h4>
                </div>
                <div class="col-md-10 foodm">
                    <div class="form-group form-group-lg form-float">
                        <div class="form-line">
                            <input type="text" name="name" id="nameEdit" class="form-control" placeholder="" maxlength="100">
                        </div>
                        <label class="font-12">Insert Country Name</label>
                    </div>
                </div>
            </div>

            <div class="col-md-12 foodm">
                <div class="col-md-2 foodm">
                    <h4>Code</h4>
                </div>
                <div class="col-md-10 foodm">
                    <div class="form-group form-group-lg form-float">
                        <div class="form-line">
                            <input type="text" name="code" id="codeEdit" class="form-control" placeholder="" maxlength="3" minlength="3">
                        </div>
                        <label class="font-12">Insert Country Symbol. For US Dollar - $</label>
                    </div>
                </div>
            </div>


        </div>

        <div class="col-md-6 foodm">

            <div class="col-md-12 foodm">
                <div class="col-md-2 foodm">
                    <h4>Show</h4>
                </div>
                <div class="col-md-10 foodm">
                    <div class="form-group form-group-lg form-float">
                        <div class="form-line">
                            <img src="" id="flgSrc" class="img-thumbnail" width="50" height="auto">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 foodm">
                <div class="col-md-2 foodm">
                    <h4>Image</h4>
                </div>
                <div class="col-md-10 foodm">
                    <div class="form-group form-group-lg form-float">
                        <div class="form-line">
                            <input type="file" name="flg" id="flg" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row clearfix">
            <div class="col-md-12 form-control-label">
                <div align="center">
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect "><h5>Save Country</h5>
                    </button>
                </div>
            </div>
        </div>

    </form>

</div>
