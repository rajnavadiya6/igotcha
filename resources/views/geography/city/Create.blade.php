<div role="tabpanel" class="tab-pane fade" id="create">

    <div id="redalert" class="alert bg-red" style='display:none;'></div>

    <form id="formcreate" method="post" enctype="multipart/form-data" action="{{route('cities.store')}}">
        @csrf
        <div class="row clearfix">

            <div class="col-md-6 foodm">

                <div class="col-md-12 foodm">
                    <div class="col-md-2 foodm">
                        <h4>Name</h4>
                    </div>
                    <div class="col-md-10 foodm">
                        <div class="form-group form-group-lg form-float">
                            <div class="form-line">
                                <input type="text" name="name" id="name" class="form-control" placeholder="" maxlength="100">
                                <label class="form-label">Insert Country Name</label>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12 foodm">
                    <div class="col-md-2 foodm">
                        <h4>Country</h4>
                    </div>
                    <div class="col-md-10 foodm">
                        <div class="form-group form-group-lg form-float">
                            <div class="form-line">
                                <select name="country_id" id="country" class="form-control">
                                    <option value="">SelectOptions</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" class="text-capitalize">{{$country->name}}</option>
                                    @endforeach
                                </select>
                                <label class="form-label">Insert Country Symbol. For US Dollar - $</label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6 foodm">

                <div class="col-md-12 foodm">
                    <div class="col-md-2 foodm">
                        <h4>Image</h4>
                    </div>
                    <div class="col-md-10 foodm">
                        <div class="form-group form-group-lg form-float">
                            <div class="form-line">
                                <input type="file" name="flg" id="flg" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row clearfix">
                <div class="col-md-12 form-control-label">
                    <div align="center">
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect "><h5>Save City</h5>
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </form>

</div>
