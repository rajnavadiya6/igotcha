@inject('util', 'App\Util')
@inject('lang', 'App\Lang')

<div class="col-md-12 " style="margin-bottom: 0px">
    <div class="col-md-4 form-control-label" style="margin-bottom: 0px">
        <label for="name"><h4>{{$label}}
                    <span class="col-red">*</span>
             
            </h4></label>
    </div>
    <div class="col-md-8" style="margin-bottom: 0px">
        <div class="form-group form-group-lg" style="margin-bottom: 0px">
            <div class="form-line" >
                <select name="{{$id}}" id="{{$id}}" multiple class="form-control show-tick" onchange="{{$onchange}};" >
                    @foreach($util->getTowns() as $key => $data)
                        <option value="{{$data->id}}" style="font-size: 16px  !important;">{{$data->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
