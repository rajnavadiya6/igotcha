@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">{{$lang->get(27)}}</h3>
            </div>
        </div>
    </div>
    <div class="body">

        <div class="card">
            <div class="header">
                <h4>
                </h4>
            </div>
            <body>
            <div class="table-responsive" style="margin-left: 5%; margin-top: 5%; margin-right: 5%" >
                <table style="margin-bottom: 5%;" width="90%">
                    <tbody id="tbody">

{{--base price--}}
                    <tr>
                        <td width="50%">
                             <label><h4>Driver Accept Charges In %: <span class="col-red">*</span></h4></label>

                            <div class="input-group">
                                <div class="form-line">
                                    <input type="number" name="driver_wallet" id="driver_wallet" class="form-control" value="" min="0" max="100">
                                </div>

                            </div>
                        </td>
                        <td width="50%"><div></div></td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <label><h4>Store Commission In %: <span class="col-red">*</span></h4></label>

                            <div class="input-group">
                                <div class="form-line">
                                    <input type="number" name="store_commission" id="store_commission" class="form-control" value="" min="0" max="100">
                                </div>

                            </div>
                        </td>
                        <td width="50%"><div></div></td>
                    </tr>
                    <tr>
                        <td width="50%">
                             <label><h4>Base Price: <span class="col-red">*</span></h4></label>

                            <div class="input-group">
                                <div class="form-line">
                                    <input type="number" id="base_price" class="form-control" value="" min="0" max="100" step="1">
                                </div>

                            </div>
                        </td>
                        <td width="50%"><div></div></td>
                    </tr>

                      <tr>
                        <td width="50%">
                        <label><h4>{{$lang->get(157)}} <span class="col-red">*</span></h4></label>
                        </td>
                    </tr>
                     <tr>
                         <td>
                        <div class="form-group form-group-lg form-float">
                            <div class="form-line">
                                <input type="number" name="fee" id="fee" class="form-control" placeholder="" min="0" step="0.01">
                                <label class="form-label">{{$lang->get(158)}}</label>
                            </div>
                        </div>
                    </div>
                    </td>
                    </tr>
{{--Tax--}}
                    <tr>
                        <td width="50%">
                            <b>{{$lang->get(318)}}:</b>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="number" id="tax" class="form-control" value="" min="0" max="100" step="1">
                                </div>
                                <p>{{$lang->get(319)}}</p>
                            </div>
                        </td>
                        <td width="50%"><div></div></td>
                    </tr>

{{--line--}}        <tr><td><hr></td><td><hr></td><td><hr></td></tr>


{{--km or miles--}}
                    <tr>
                        <td width="50%">
                            <b>{{$lang->get(320)}}:</b>
                                <div class="form-group form-group-lg form-float">
                                    <select name="distanceUnit" id="distanceUnit" class="form-control show-tick ">
                                        @if ($distanceUnit == 'km')
                                            <option value="km" style="font-size: 16px  !important;" selected>Km</option>
                                            <option value="mi" style="font-size: 16px  !important;">Miles</option>
                                        @else
                                            <option value="mi" style="font-size: 16px  !important;" selected>Miles</option>
                                            <option value="km" style="font-size: 16px  !important;">Km</option>
                                        @endif
                                    </select>
                                    <label class="font-12 font-bold"><p>{{$lang->get(321)}}</p></label>
                            </div>
                        </td>
                        <td width="50%"><div></div></td>
                    </tr>
{{--line--}}        <tr><td><hr></td><td><hr></td><td><hr></td></tr>

{{--timezone--}}
                    <tr>
                        <td width="50%">
                            <b>{{$lang->get(468)}}:</b>   {{--"Set Time Zone",--}}
                            <div class="form-group form-group-lg form-float">
                                <select name="timezone" id="timezone" class="form-control show-tick" data-size="5">
                                    @foreach($timezonesArray as $key => $data)
                                        @if ($data == $timezone)
                                            <option value="{{$data}}" style="font-size: 16px  !important;" selected>{{$data}}</option>
                                        @else
                                            <option value="{{$data}}" style="font-size: 16px  !important;">{{$data}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label class="font-12 font-bold"><p>{{$lang->get(469)}}</p></label>    {{--"Select default Time Zone for Admin Panel",--}}
                            </div>
                        </td>
                        <td width="50%"><div></div></td>
                    </tr>
{{--line--}}        <tr><td><hr></td><td><hr></td><td><hr></td></tr>


{{--Firebase Cloud Messaging Key--}}

                    <th colspan="3">
                        <b>{{$lang->get(322)}}:</b>
                        <div class="input-group">
                            <div class="form-line">
                                <input type="text" id="firebase" class="form-control">
                            </div>
                            <p style="font-weight: 400;">{{$lang->get(323)}}</p>
                        </div>
                    </th>

{{--line--}}        <tr><td><hr></td><td><hr></td><td><hr></td></tr>

{{--Google Maps Api Key--}}

                    <tr>
                        <td width="50%">
                        <b>Google Maps Api Key:</b>
                        <div class="input-group">
                            <div class="form-line">
                                <input type="text" id="mapapikey" class="form-control">
                            </div>
                            <p style="font-weight: 400;">{{$lang->get(324)}}</p>
                        </div>
                        </td>
                        <td width="5%">
                        </td>
                        <td width="45%">
                            {{$lang->get(325)}}
                            <a href="https://developers.google.com/maps/gmp-get-started">https://developers.google.com/maps/gmp-get-started.</a>
                            <br>
                            {{$lang->get(326)}}
                            <a href="https://www.valeraletun.ru/codecanyon/delivery/documentation/index.html#/?id=create-your-own-google-maps-api-key">documentation</a>
                        </td>
                    </tr>
                     <tr>
                        <td width="50%">
                            <b>Store App Force Update:</b>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="number" id="store_app_force_update" class="form-control" value="" min="0" >
                                </div>
                            </div>
                        </td>

                    </tr>
                     <tr>
                        <td width="50%">
                            <b>Driver App Force Update:</b>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="number" id="driver_app_force_update" class="form-control" value="" min="0" >
                                </div>
                            </div>
                        </td>

                    </tr>
                     <tr>
                        <td width="50%">
                            <b>User App Force Update:</b>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="number" id="user_app_force_update" class="form-control" value="" min="0" >
                                </div>
                            </div>
                        </td>

                    </tr>

{{--save--}}
                    <tr>
                        <td width="70%">
                            <div align="right">
                                <div class="btn btn-primary waves-effect" onClick="onSave()"><h4>{{$lang->get(142)}}</h4></div>
                            </div>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

{{--line--}}        <hr>

            <div align="center">
            <form id="form" method="post" action="{{url('settingsSetLang')}}"  >
                @csrf
                <div class="row clearfix">
                    <div class="col-md-4 form-control-label">
{{--Select Language for Admin Panel--}}
                        <label for="name"><h4>{{$lang->get(436)}}</h4></label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-group-lg form-float">
                            <div class="form-line">
                                <select name="newLang" id="newLang" class="form-control bs-searchbox " style="font-size: 26px  !important; ">
                                    @foreach($langs as $key => $data)
                                        @if ($defLang == $data["file"])
                                            <option value="{{$data['file']}}" selected style="font-size: 18px  !important;" >{{$data["name"]}} - {{$data["name2"]}}</option>
                                        @else
                                            <option value="{{$data['file']}}" style="font-size: 18px  !important;">{{$data["name"]}} - {{$data["name2"]}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

{{--Set language--}}
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect "><h5>{{$lang->get(437)}}</h5></button>
                    </div>
                </div>

            </form>
            </div>

            </body>
        </div>
    <script>

        var tax = document.getElementById('tax');
        tax.addEventListener('input',  function(e){inputHandler(e, tax, 0, 100);});

        // init parameters

        document.getElementById("tax").value = "{{$tax}}" ;
        document.getElementById("firebase").value = "{{$firebase}}" ;
        document.getElementById("mapapikey").value = "{{$mapapikey}}" ;
        document.getElementById("base_price").value = "{{$basePrice}}" ;
        document.getElementById("driver_wallet").value = "{{$driver_wallet_charge}}" ;
        document.getElementById("store_commission").value = "{{$store_commission}}" ;
        document.getElementById("fee").value = "{{$fee}}";
        document.getElementById("driver_app_force_update").value="{{$driver_app_force_update}}";
        document.getElementById("store_app_force_update").value="{{$store_app_force_update}}";
        document.getElementById("user_app_force_update").value="{{$user_app_force_update}}";

        function onSave(){
            var firebase = document.getElementById("firebase").value;
            var mapapikey = document.getElementById("mapapikey").value;
            var distanceUnit = document.getElementById("distanceUnit").value;
            var base_price=document.getElementById("base_price").value;
            var driver_wallet_charge = document.getElementById("driver_wallet").value;
            var fee=document.getElementById("fee").value;
            var driver_app_force_update=document.getElementById("driver_app_force_update").value;
            var store_app_force_update=document.getElementById("store_app_force_update").value;
            var user_app_force_update=document.getElementById("user_app_force_update").value;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'POST',
                url: '{{ url("settingschange") }}',
                data: {
                    tax: document.getElementById("tax").value,
                    distanceUnit: distanceUnit,
                    firebase: firebase,
                    mapapikey : mapapikey,
                    timezone: document.getElementById("timezone").value,
                    base_price:base_price,
                    driver_wallet_charges:document.getElementById("driver_wallet").value,
                    store_commission:document.getElementById("store_commission").value,
                    fee:fee,
                    user_app_force_update:user_app_force_update,
                    store_app_force_update:store_app_force_update,
                    driver_app_force_update:driver_app_force_update
                },
                success: function (data){
                    console.log(data);
                    showNotification("bg-teal", "Settings Saved", "bottom", "center", "", "");
                },
                error: function(e) {
                    console.log(e);
                }}
            );
        }

    </script>


@endsection

@section('content2')

@endsection
