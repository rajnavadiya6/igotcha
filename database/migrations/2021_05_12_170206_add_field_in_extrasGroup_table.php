<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldInExtrasGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('extrasgroup', function (Blueprint $table) {
            $table->integer('select_multiple')->default(false);
            $table->integer('plus_minus_button')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extrasgroup', function (Blueprint $table) {
            $table->dropColumn('select_multiple');
            $table->dropColumn('plus_minus_button');
        });
    }
}
