<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_extras', function (Blueprint $table) {
            $table->id();
            $table->foreignId('food_id');
            $table->foreign('food_id')->references('id')->on('foods')->cascadeOnDelete();
            $table->foreignId('extra_id');
            $table->foreign('extra_id')->references('id')->on('extrasgroup')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_extras');
    }
}
