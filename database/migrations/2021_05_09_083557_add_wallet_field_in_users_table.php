<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWalletFieldInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('driver_wallet')->nullable();
        });
        \Illuminate\Support\Facades\DB::table('settings')->insert(['param' => 'driver_wallet_charges', 'value' => '0', 'created_at' => new \DateTime(), 'updated_at' => new \DateTime(),]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('driver_wallet');
        });
    }
}
