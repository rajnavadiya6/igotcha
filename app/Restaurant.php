<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{

    public function town()
    {
        return $this->hasOne(Town::class);
    }

    public function city()
    {
        return $this->hasOne(City::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class);
    }
}
