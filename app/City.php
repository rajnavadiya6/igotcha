<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class City extends Model
{
    protected $fillable = [
        'name',
        'country_id',
        'flg',
    ];

    protected $appends = [
        'flg_url',
    ];

    public function RemovePhoto()
    {
        try {
            if (Storage::disk('public')->exists($this->flg)) {
                Storage::disk('public')->delete($this->flg);
            }
        } catch (\Throwable $th) {

        }
    }

    public function getFlgUrlAttribute()
    {
        return asset($this->flg ? Storage::disk('local')->url($this->flg) : 'https://ui-avatars.com/api/?name=' . urlencode($this->name) . '&color=7F9CF5&background=EBF4FF');
    }

    public function towns()
    {
        return $this->hasMany(Town::class)->latest();
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function Restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }
}
