<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function towns()
    {
        return $this->belongsToMany(Town::class,'town_category');
    }
}
