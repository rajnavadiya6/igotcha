<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Town extends Model
{
    protected $fillable = [
        'name',
        'city_id',
        'flg',
    ];

    protected $appends = [
        'flg_url',
    ];

    public function RemovePhoto()
    {
        try {
            if (Storage::disk('public')->exists($this->flg)) {
                Storage::disk('public')->delete($this->flg);
            }
        } catch (\Throwable $th) {

        }
    }

    public function getFlgUrlAttribute()
    {
        return asset($this->flg ? Storage::disk('local')->url($this->flg) : 'https://ui-avatars.com/api/?name=' . urlencode($this->name) . '&color=7F9CF5&background=EBF4FF');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function Restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }
}
