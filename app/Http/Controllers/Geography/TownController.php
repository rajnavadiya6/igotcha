<?php

namespace App\Http\Controllers\Geography;

use App\City;
use App\Http\Controllers\Controller;
use App\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TownController extends Controller
{
    public function index(Town $town, City $city)
    {
        return view('geography.town.Index', [
            'towns' => $town->all(),
            'cities' => $city->all(),
        ]);
    }

    public function store(Request $request,Town $town)
    {
        $request->validate([
            'name' => 'required|min:3|max:100|unique:towns,name',
            'city_id' => 'required|exists:cities,id',
            'flg' => 'mimes:jpg,jpeg,png|max:1000',
        ]);

        $town->create([
            'name' => $request->name,
            'city_id' => $request->city_id,
            'flg' => $request->hasFile('flg')?$request->file('flg')->store('countries', 'public'):null,
        ]);

        return back()->with('success', 'Town created successfully');
    }

    public function update(Request $request, Town $town)
    {
        $town = $town->find($request->id);

        $request->validate([
            'name' => 'required|min:3|max:100|unique:towns,name,'.$town->id,
            'city_id' => 'required|exists:cities,id',
        ]);

        if ($request->hasFile('flg')) {

            $validator = Validator::make($request->all(), [
                'flg' => 'required|mimes:jpg,jpeg,png|max:1000',
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            }

            $town->RemovePhoto();

            $town->update([
                'flg' => $request->file('flg')->store('countries', 'public'),
            ]);
        }

        $town->update([
            'name' => $request->name,
            'city_id' => $request->city_id
        ]);

        return back()->with('success', 'Town updated successfully');
    }

}
