<?php

namespace App\Http\Controllers\Geography;

use App\Country;
use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\Return_;

class CountryController extends Controller
{

    public function index(Country $country)
    {
        return view('geography.country.Index', [
            'countries' => $country->all()
        ]);
    }

    public function store(Request $request, Country $country)
    {
        $request->validate([
            'name' => 'required|min:3|max:100|unique:countries,name',
            'code' => 'required|min:3|max:3',
            'flg' => 'mimes:jpg,jpeg,png|max:1000',
        ]);

        $country->create([
            'name' => $request->name,
            'code' => $request->code,
            'flg' => $request->hasFile('flg')?$request->file('flg')->store('countries', 'public'):null,
        ]);

        return back()->with('success', 'country created successfully');
    }

    public function update(Request $request, Country $country)
    {
        $country = $country->find($request->id);

        $request->validate([
            'name' => 'required|min:3|max:100|unique:countries,name,' . $country->id,
            'code' => 'required|min:3|max:3',
        ]);

        if ($request->hasFile('flg')) {

            $validator = Validator::make($request->all(), [
                'flg' => 'required|mimes:jpg,jpeg,png|max:1000',
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            }

            $country->RemovePhoto();

            $country->update([
                'flg' => $request->file('flg')->store('countries', 'public'),
            ]);
        }

        $country->update([
            'name' => $request->name,
            'code' => $request->code,
        ]);

        return back()->with('success', 'country updated successfully');

    }

    public function showCities(Request $request, Country $country)
    {
        $request->validate([
            'id' => 'required|exists:countries,id'
        ]);

        $country = Country::find($request->id);

        $html = '';
        $html .= '<div class="col-md-3 foodm">';
        $html .= '<label for="lat"><h4>City<span class="col-red">*</span></h4></label>';
        $html .= '</div>';
        $html .= '<div class="col-md-9 foodm">';
        $html .= '<div class="form-group form-group-lg form-float">';
        $html .= '<div class="form-line">';
        $html .= '<select name="city_id" id="city"  class="form-control">';
        $html .= '<option value="">Select Option</option>';

        foreach ($country->cities as $key => $city) {

            if(isset($request->city_id) && $request->city_id == $city->id){

                $html .= "<option value='$city->id' selected>$city->name</option>";

            }else{

                $html .= "<option value='$city->id' >$city->name</option>";
            }

        }
        if (empty($country->cities->all())) {
            $html .= "<option value=''>NO DATA FOR COUNTRY</option>";
        }

        $html .= '</select>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

}
