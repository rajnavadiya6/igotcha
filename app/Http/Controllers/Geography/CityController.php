<?php

namespace App\Http\Controllers\Geography;

use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    public function index(City $city, Country $country)
    {
        return view('geography.city.Index', [
            'cities' => $city->all(),
            'countries' => $country->all()
        ]);
    }

    public function store(Request $request, City $city)
    {
        $request->validate([
            'name' => 'required|min:3|max:100|unique:cities,name',
            'country_id' => 'required|exists:countries,id',
            'flg' => 'mimes:jpg,jpeg,png|max:1000',
        ]);

        $city->create([
            'name' => $request->name,
            'country_id' => $request->country_id,
            'flg' => $request->hasFile('flg')?$request->file('flg')->store('cities', 'public'):null,
        ]);

        return back()->with('success', 'country created successfully');
    }

    public function update(Request $request, City $city)
    {

        $city = $city->find($request->id);

        $request->validate([
            'name' => 'required|min:3|max:100|unique:cities,name,' . $city->id,
            'country_id' => 'required|exists:countries,id',
        ]);

        if ($request->hasFile('flg')) {

            $validator = Validator::make($request->all(), [
                'flg' => 'required|mimes:jpg,jpeg,png|max:1000',
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            }

            $city->RemovePhoto();

            $city->update([
                'flg' => $request->file('flg')->store('cities', 'public'),
            ]);
        }

        $city->update([
            'name' => $request->name,
            'country_id' => $request->country_id
        ]);

        return back()->with('success', 'city updated successfully');
    }

    public function showTowns(Request $request, City $city)
    {
        $request->validate([
            'id' => 'required|exists:cities,id'
        ]);

        $city = $city->find($request->id);

        $html = '';
        $html .= '<div class="col-md-3 foodm">';
        $html .= '<label for="lat"><h4>Towns<span class="col-red">*</span></h4></label>';
        $html .= '</div>';
        $html .= '<div class="col-md-9 foodm">';
        $html .= '<div class="form-group form-group-lg form-float">';
        $html .= '<div class="form-line">';
        $html .= '<select name="town_id" id="town" class="form-control">';
        $html .= '<option value="">Select Option</option>';

        foreach ($city->towns as $key => $town) {

            if(isset($request->town_id) && $request->town_id == $town->id){

                $html .= "<option value='$town->id' selected>$town->name</option>";

            }else{

                $html .= "<option value='$town->id'>$town->name</option>";

            }


        }

        if (empty($city->towns->all())) {
            $html .= "<option value=''>NO DATA FOR CITY</option>";
        }

        $html .= '</select>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}
