<?php
namespace App\Http\Controllers\API\owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB;
use App\Logging;
use Auth;
use App\Lang;
use App\Http\Controllers\MessagingController;
use App\Http\Controllers\OrdersController;
use App\UserInfo;

class OwnerOrdersController extends Controller
{
    public function ordersList()
    {

        if (UserInfo::getUserRoleId() == 2) // manager
            $orders = DB::table('manager_rest')->where('manager_rest.user', '=', Auth::user()->id)->join("orders", 'orders.restaurant', '=', 'manager_rest.restaurant')->where("send", "1")->join("users", 'users.id', '=', 'orders.user')->
                select('orders.*', 'users.name as userName')->limit(100)->orderBy('updated_at', 'desc')->get();
        else
            $orders = DB::table('orders')->where("send", "1")->join("users", 'users.id', '=', 'orders.user')->
                    select('orders.*', 'users.name as userName')->limit(100)->orderBy('updated_at', 'desc')->get();
        foreach ($orders as &$value) {
            $value->driver_name='';
            if($value->status=='7' || $value->status=='4' || $value->status=='5'){
                $res=DB::table('request_order')->select('users.name as driver_name')->join('users','users.id','driver_id')->where('order_id',$value->id)->first();
                if($res)
                $value->driver_name=$res->driver_name;
            }
            $value->ordersData = DB::table('ordersdetails')->where("order", $value->id)->get();
            $rest = DB::table('restaurants')->where("id", $value->restaurant)->get()->first();
            if ($rest != null){
                $value->addressDest = $rest->address;
                $value->latRest = $rest->lat;
                $value->lngRest = $rest->lng;
            }
        }
        $orderStatus = DB::table('orderstatuses')->get();
        $currencies = DB::table('settings')->where('param', '=', "default_currencies")->get()->first()->value;
        $rightSymbol = DB::table('settings')->where('param', '=', "rightSymbol")->get()->first()->value;
        $distanceUnit = DB::table('settings')->where('param', '=', "distanceUnit")->get()->first()->value;
        $drivers = DB::table('users')->where('role', '=', "3")->select('id', 'name', 'imageid', 'phone', 'active')->get();
        $basePrice=DB::table('settings')->where('param', '=', "basePrice")->get()->first()->value;
        $fee=DB::table('settings')->where('param', '=', "fee")->get()->first()->value;
        
        $response = [
            'error' => '0',
            'id' => "",
            'orders' => $orders,
            'orderStatus' => $orderStatus,
            'currency' => $currencies,
            'rightSymbol' => $rightSymbol,
            'distanceUnit' => $distanceUnit,
            'googleApiKey' => DB::table('settings')->where('param', '=', "mapapikey")->get()->first()->value,
            'drivers' => $drivers,
            'images' => DB::table('image_uploads')->select('id', 'filename')->get(),
            'basePrice'=>$basePrice,
            'fee'=>$fee
        ];
        return response()->json($response, 200);
    }

    public function changeStatus(Request $request)
    {
        if (!Auth::check())
            return \Redirect::route('/');

        $order_id = $request->input('id') ?: "";
        $status = $request->input('status') ?: "";

        $values = array('status' => $status, 'updated_at' => new \DateTime());
        DB::table('orders')->where('id',$order_id)->update($values);

        $drivers = DB::table('users')->where('active','1')->where('role', '=', "3")->select('id', 'name', 'imageid', 'phone', 'active','fcbToken')->get();
        if($status=="7")
        {
        if(count($drivers)>0)
        {
            $defaultImage = DB::table('settings')->where('param', '=', "notify_image")->get()->first()->value;
            //\Log::info("if status==>".$status);
          foreach ($drivers as $key => $driver) {
              $driver=get_object_vars($driver);
              $duplicateRec=DB::table("request_order")->where('order_id',$order_id)->where('driver_id',$driver['id'])->count();
              if($duplicateRec==0){
                $myRequest = new \Illuminate\Http\Request();
        $myRequest->setMethod('POST');
        $myRequest->request->add(['user' => $driver['id']]);
        $myRequest->request->add(['title' => Lang::get(538)]); // New order received
        $myRequest->request->add(['text' => Lang::get(536) . $order_id . Lang::get(537)]); //  "New order #",  was received
        $myRequest->request->add(['imageid' => $defaultImage]);
        MessagingController::sendNotify($myRequest);
            
             $values = array(
            'order_id' => $order_id, 'status' => "15", 'driver' => $driver['id'],
            'comment' => "",
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        );
        DB::table('ordertimes')->insert($values);

        
        
        $valuesOrder = array(
            'order_id' => $order_id,    'driver_id' => $driver['id'],
            
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        );
        DB::table('request_order')->insert($valuesOrder);
        }
          }   
        }
        
        }
        else
        {
        //
        // Send Notifications to user
        //
        $myRequest = new \Illuminate\Http\Request();
       // \Log::info("else status==>".$status);
        $myRequest->setMethod('POST');
        $id = DB::table('orders')->where('id',$order_id)->get()->first()->user;
        $myRequest->request->add(['user' => $id]);
        $myRequest->request->add(['title' => Lang::get(470)]); // 'Order status changed',
        $status_text = DB::table('orderstatuses')->where('id',$status)->get()->first()->status;
        $myRequest->request->add(['text' => Lang::get(475) . $order_id . Lang::get(476) . $status_text]);  // "You order #",  ' was '
        $defaultImage = DB::table('settings')->where('param', '=', "notify_image")->get()->first()->value;
        $myRequest->request->add(['imageid' => $defaultImage]);
        MessagingController::sendNotify($myRequest);

        //
        // save to OrdersTime details
        //
        $values = array(
            'order_id' => "$order_id", 'status' => "$status", 'driver' => 0,
            'comment' => "",
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        );
        DB::table('ordertimes')->insert($values);
        }
        return response()->json([
            'error'=>"1",
        ]);
    }

    public function changeDriver(Request $request)
    {
        if (!Auth::check())
            return \Redirect::route('/');

        return OrdersController::changeDriver($request);
    }
}
