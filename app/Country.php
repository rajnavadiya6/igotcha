<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Country extends Model
{
    protected $fillable = [
        'name',
        'code',
        'flg',
    ];

    protected $appends = [
        'flg_url',
    ];

    public function RemovePhoto()
    {
        try {
            if (Storage::disk('public')->exists($this->flg)) {
                Storage::disk('public')->delete($this->flg);
            }
        } catch (\Throwable $th) {

        }
    }

    public function getFlgUrlAttribute()
    {
        return asset($this->flg ? Storage::disk('local')->url($this->flg) : 'https://ui-avatars.com/api/?name=' . urlencode($this->name) . '&color=7F9CF5&background=EBF4FF');
    }

    public function cities()
    {
        return $this->hasMany(City::class)->latest();
    }

    public function Restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

}
